# Canal Project: Server and Admin Bundled

- [Canal](https://bitbucket.org/bemit_eu/canal)
- [Canal: Asset](https://bitbucket.org/bemit_eu/canal-asset)
- [Canal: View](https://bitbucket.org/bemit_eu/canal-view)
- [Canal: Structure](https://bitbucket.org/bemit_eu/canal-structure)
- [Canal: Admin](https://bitbucket.org/bemit_eu/canal-admin)

## Setup

For PHP or NodeJS local installation and setup:

```bash
git clone --recurse-submodules -j8 https://bitbucket.org/bemit_eu/canal-bundle.git

# Structure / Setup Flat-File CMS
cd canal-bundle/structure
composer install

## when on windows, uses windows subsystem linux
npm run start-win
## when on mac or linux
npm run start-uni

#
# Admin
cd canal-bundle/admin
npm i

## start dev-server
npm start
## create a production build
npm run build
```
    
## Docker

Development setup with docker-compose, still in progress.

```bash
git clone --recurse-submodules -j8 https://bitbucket.org/bemit_eu/canal-bundle.git

cd canal-bundle

# Structure: temporary dependency installation
cd structure
composer install
cd ../

# spin up containers
docker-compose up
# Frontend: localhost:25020
# Admin: localhost:25021
docker-compose up --build
docker-compose up admin
docker-compose up canal
```

Admin Boot needs a while currently, during npm i on start only the result is logged, not the progress.

## Licence

This project is free software distributed under the terms of two licences, the CeCILL-C and the GNU Lesser General Public License. You can use, modify and/ or redistribute the software under the terms of CeCILL-C (v1) for Europe or GNU LGPL (v3) for the rest of the world.

This file and the LICENCE.* files need to be distributed and not changed when distributing.
For more informations on the licences which are applied read: [LICENCE.md](LICENCE.md)


# Copyright

2017-2019 | [bemit UG (haftungsbeschränkt)](https://bemit.eu) - project@bemit.codes

Maintainer: [Michael Becker](https://mlbr.xyz)